@extends('layouts.app')

@section('live-preview-btn')
    <div class="text-center mt-4">
        <button class="btn btn-danger" id="preview-btn" data-toggle="modal" data-target="#previewModal" style="width: 100%">Live Preview</button>
    </div>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Add Post</div>
        <div class="card-body">
            <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text"
                           value="{{ old('title') }}"
                           class="form-control @error('title') is-invalid @enderror"
                           id="title"
                           name="title">
                    @error('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="category_id">Category</label>
                    <select name="category_id" id="category_id" class="form-control select2">
                        <option value="" selected disabled>SELECT</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="tags">Tags</label>
                    <select name="tags[]" id="tags" class="form-control select2" multiple>
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="excerpt">Excerpt</label>
                    <textarea name="excerpt"
                              id="excerpt"
                              class="form-control @error('excerpt') is-invalid @enderror"
                              rows="4">{{ old('excerpt') }}</textarea>
                    @error('excerpt')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <input type="hidden" id="content" name="content">
                    <trix-editor input="content"></trix-editor>

                    @error('content')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="published_at">Published At</label>
                    <input type="text"
                           value="{{ old('published_at') }}"
                           class="form-control"
                           name="published_at" id="published_at">
                    @error('published_at')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file"
                            onchange="readURL(this);"
                           class="form-control @error('image') is-invalid @enderror"
                           name="image" id="image">
                    @error('image')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Add Post</button>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
            <div class="modal-content" style="height: 70vh">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Blog Preview</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="max-width: 100%; height:40vh;">
                    <div class="blog-three-mini">
                        <div class="clearfix">
                            <div><h2 class="color-dark pull-left" id="preview-title">Title...</h2></div>
                        </div>
                        <div class="blog-three-attrib">
                            <div><i class="fa fa-calendar"></i>Live Preview</div> |
                            <div><i class="fa fa-pencil"></i><a href="#" id="preview-author">{{ Auth::user()->name }}</a></div> |
                            <div>
                                Share:  <a href="#"><i class="fa fa-facebook-official"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>

                        <img src="https://via.placeholder.com/1920x1080" alt="Blog Image" id="img-preview" class="img-responsive" width="100%">
                        <div class="lead mt25" id="preview-content">
                            Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content, Some Content,
                        </div>

                        <div class="blog-post-read-tag mt50">
                            <i class="fa fa-tags"></i> Tags:
                                <div id="preview-tag-parent" style="display: inline-block">

                                </div>
                        </div>

                        <div class="blog-post-author mb50 pt30 bt-solid-1">
                            <img src="{{ \Thomaswelton\LaravelGravatar\Facades\Gravatar::src(Auth::user()->email) }}" class="img-circle" alt="image">
                            <span class="blog-post-author-name">{{ Auth::user()->name }}</span> <a href="https://twitter.com/booisme"><i class="fa fa-twitter"></i></a>
                            <p>
                                {{ Auth::user()->about == ""? "N.A" : Auth::user()->about }}
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="{{ asset('assets/js/preview/app.js') }}"></script>
    <script>
        flatpickr("#published_at", {
            enableTime: true
        });
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: "select..."
            });
        });
    </script>
    @include('layouts.blog/_js')

@endsection
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
    @include('layouts.blog/_css')
@endsection
