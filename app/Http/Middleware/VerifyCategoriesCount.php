<?php

namespace App\Http\Middleware;

use App\Category;
use Closure;

class VerifyCategoriesCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         * Category::all()->count() dont do this because it will fetch n records.
         * and it will bring n and use collection count
         */
        if(Category::count() === 0){
            session()->flash('error', 'Minimum one category must exist to create a post!');
            return redirect(route('categories.index'));
        }
        return $next($request);
    } //next is to register the middleware
}
