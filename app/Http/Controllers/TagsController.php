<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tag\CreateTagRequest;
use App\Http\Requests\Tag\UpdateTagRequest;
use App\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tag $tag)
    {
        $tags = Tag::all();
        return view('tags.index', compact([
            'tags'
        ]));
    }
    public function create()
    {
        return view('tags.create');
    }
    public function store(CreateTagRequest $request)
    {
        // 1. Validate Data - done

        // 2. Store the data in database
        Tag::create([
            'name'=>$request->name
        ]);

        // 3. Session set something and then return a view
        session()->flash('success', 'Tag Added Successfully');
        return redirect(route('tags.index'));
    }
    public function show(Tag $tag)
    {
        //
    }
    public function edit(Tag $tag)
    {
        return view('tags.edit', compact([
            'tag'
        ]));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $tag->name = $request->name;
        $tag->save();
        session()->flash('success', 'Tag Updated Successful!');
        return redirect(route('tags.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        if($tag->posts->count()>0)
        {
            session()->flash('error', 'Tag cannot be deleted as it is associated with some post!');    
            return redirect()->back();
        }
        $tag->delete();
        session()->flash('success', 'Tag Deleted Successfully!');
        return redirect(route('tags.index'));
    }
}
