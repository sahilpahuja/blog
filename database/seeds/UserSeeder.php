<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'sahilpahuja@studylinkclasses.com')->get()->first();
        if(!$user){
            User::create([
                'name'=>'Sahil Pahuja',
                'email'=>'sahilpahuja@studylinkclasses.com',
                'password'=>Hash::make('abcd1234'),
                'role'=>'admin'
            ]);
        } else {
            $user->update(['role'=>'admin']);
        }

        User::create([
            'name'=>'Mihir Chichria',
            'email'=>'mihir@studylinkclasses.com',
            'password'=>Hash::make('abcd1234'),
        ]);

        User::create([
            'name'=>'Jaanvi RIjhwani',
            'email'=>'jaanvi@studylinkclasses.com',
            'password'=>Hash::make('abcd1234'),
        ]);
        
    }
}
